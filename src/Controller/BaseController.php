<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ContactType;
use App\Form\AvisType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use App\Entity\Contact;
use App\Entity\Avis;


class BaseController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('base/index.html.twig', [
            'controller_name' => 'BaseController', // suppression de la variable passée à la vue
        ]);

    }
   #[Route('/contact', name: 'contact')]
    public function contact(Request $request, MailerInterface $mailer): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);

        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if ($form->isSubmitted()&&$form->isValid()){   
                $email = (new TemplatedEmail())
                ->from($contact->getEmail())
                ->to('reply@nuage-pedagogique.fr')
                ->subject($contact->getSujet())
                ->htmlTemplate('emails/email.html.twig')
                ->context([
                    'nom'=> $contact->getNom(),
                    'sujet'=> $contact->getSujet(),
                    'message'=> $contact->getMessage()
                ]); 
 
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush(); 
                $mailer->send($email);
                $this->addFlash('notice','Message envoyé');
                return $this->redirectToRoute('contact'); 
 

              
                        
            }
        }

        return $this->render('base/contact.html.twig', [
            'form' => $form->createView()
            
        ]);
    }

    #[Route('/avis', name: 'avis')]
    public function avis(Request $request): Response{
        $avis = new Avis();
        $form = $this->createForm(AvisType::class, $avis);

        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if ($form->isSubmitted()&&$form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($avis);
                $em->flush(); 
                $this->addFlash('notice','Message envoyé');
                return $this->redirectToRoute('avis'); 
            }
        }

        return $this->render('base/avis.html.twig', [
            'form' => $form->createView()
        ]);
    }


    #[Route('/mentionLegal', name: 'mentionLegal')]
    public function mentionLegal(): Response
    {
        return $this->render('base/mentionLegal.html.twig', [
            'controller_name' => 'BaseController', // suppression de la variable passée à la vue
        ]);

    }

    #[Route('/Apropos', name: 'Apropos')]
    public function Apropos(): Response
    {
        return $this->render('base/Apropos.html.twig', [
            'controller_name' => 'BaseController', // suppression de la variable passée à la vue
        ]);

    }
} 

